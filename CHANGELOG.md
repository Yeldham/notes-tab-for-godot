# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.1.0/)
and this project adheres to
[Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [2.0.0] - 2023-12-07

### Changed

- Update to Godot Engine 4.2.

## [1.2.3] - 2020-01-20

### Changed

- Trivial internal changes.

## [1.2.2] - 2019-08-28

### Changed

- Trivial internal changes.

## [1.2.1] - 2018-12-05

### Fixed

- Fix editor settings not being applied on startup.

## [1.2.0] - 2018-11-26

### Changed

- Remember the text's cursor position between sessions.

### Fixed

- Fix mouse cursor not turning into the "I-Beam" shape when hovering the notes.

## [1.1.0] - 2018-10-28

### Changed

- Make notes follow some of the editor settings (cursor, word wrapping, and
scrolling), for consistency sake.
- The license is now the [GPL v3](https://www.gnu.org/licenses/gpl.html),
as it makes more sense for an asset that is for the editor only.

[2.0.0]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.2.3...v2.0.0
[1.2.3]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.2.2...v1.2.3
[1.2.2]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.2.1...v1.2.2
[1.2.1]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.2.0...v1.2.1
[1.2.0]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.1.0...v1.2.0
[1.1.0]: https://codeberg.org/Yeldham/notes-tab-for-godot/compare/v1.0.0...v1.1.0
