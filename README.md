# Notes Tab for the Godot Engine

This is the git repository for the asset. For an easier integration with your
project, it's recommended to download it via its
[Asset Library page](https://godotengine.org/asset-library/asset/261).

## License

This asset is licensed under the
[GPL v3+](https://www.gnu.org/licenses/gpl.html).
